package com.example.jpadatabase.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.jpadatabase.entity.Channel;
import com.example.jpadatabase.repository.ChannelRepository;

@RestController
@RequestMapping("/channels")
public class ChannelController {

	@Autowired
	ChannelRepository channelRepository;

	@Cacheable(value = "channelsname")
	@GetMapping("/channel")
	public /* ResponseEntity< */List<Channel> getChannels() {
		List<Channel> list = (List<Channel>) channelRepository.findAll();
		System.out.println(list);
		return list;
	}

}
