package com.example.jpadatabase.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.jpadatabase.entity.Channel;

public interface ChannelRepository  extends JpaRepository<Channel, String>{
}
