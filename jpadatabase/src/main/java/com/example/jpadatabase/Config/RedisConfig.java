package com.example.jpadatabase.Config;

import java.time.Duration;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;

@Configuration
@EnableCaching
@PropertySource("classpath:application.properties")
public class RedisConfig {
   
//	For custom Configuration defined in application.Properties
	/*@Value(value = "${spring.redis.host}")
	private String hostName;

	@Value(value = "${spring.redis.port}")
	private Integer port;

	@Value(value = "${spring.redis.password}")
	private String password;*/

//	 @Autowired
//	   private Environment env;	

	@Bean
	public LettuceConnectionFactory redisConnectionFactory() {
		// For custom configuration for redis server
//		RedisStandaloneConfiguration redisConf = new RedisStandaloneConfiguration();
//		redisConf.setHostName(hostName);
//		redisConf.setPort(port);
//		redisConf.setPassword(RedisPassword.of(password));
		return new LettuceConnectionFactory();
	}

	@Bean
	public RedisCacheConfiguration cacheConfiguration() {
		RedisCacheConfiguration cacheConfig = RedisCacheConfiguration.defaultCacheConfig()
				.entryTtl(Duration.ofSeconds(600)).disableCachingNullValues();
		return cacheConfig;
	}

	@Bean
	public RedisCacheManager cacheManager() {
		RedisCacheManager rcm = RedisCacheManager.builder(redisConnectionFactory()).cacheDefaults(cacheConfiguration())
				.transactionAware().build();
		return rcm;
	}
	
//	For Jedis Configuration
/*
	@Autowired
	 private RedisConfigSerializer redisSerializer;
	
	 @Bean
	    JedisConnectionFactory jedisConnectionFactory() {
	      JedisConnectionFactory factory = new JedisConnectionFactory();
	      factory.setHostName("localhost");
	      factory.setPort(6379);
	      factory.setUsePool(true);
	      return factory;
	    }

	    @Bean
	    public RedisTemplate<String, Channel> redisTemplate() {
	      RedisTemplate<String, Channel> redis = new RedisTemplate<>();
	      redis.setConnectionFactory(jedisConnectionFactory());
	      redis.setKeySerializer(new StringRedisSerializer());
	      redis.setValueSerializer(redisSerializer);
	      return redis;
	    }
	    
	    public RedisCacheManager redisCacheManager()
	    {
	    	RedisCacheManager cache=new RedisCacheManager(redisTemplate());
	    	cache.setTransactionAware(true);
	    	return cache;
	    }
	    */
}
