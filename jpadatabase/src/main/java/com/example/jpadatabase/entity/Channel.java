package com.example.jpadatabase.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="channel")
public class Channel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	Integer id;
	@Column(name="channel_name")
	String channelName;
	@Column(name="uuid")
	String uuid;
	@Column(name="description")
	String description;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/*@Override
    public String toString() {
        return String.format(
                "id[id=%d, channelName='%s', uuid='%s',description='%s']",
                id, channelName, uuid,description);
    }*/
}
