package com.example.jpadatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages="com.example.jpadatabase.repository")
@SpringBootApplication
@EnableCaching
public class JpadatabaseApplication {
	
	  
	public static void main(String[] args) {
		SpringApplication.run(JpadatabaseApplication.class, args);
	}
	
}
