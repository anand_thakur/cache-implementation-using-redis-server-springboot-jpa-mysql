//package com.example.jpadatabase;
//
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.client.RestTemplate;
//
//import com.example.jpadatabase.entity.Channel;
//
//public class JpadatabaseApplicationTests {
//
//	public void getAllChannels()
//	{
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//	        RestTemplate restTemplate = new RestTemplate();
//		String url = "http://localhost:8080/channels/channel";
//	        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
//	        ResponseEntity<Channel[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Channel[].class);
//	        Channel[] articles = responseEntity.getBody();
//	        for(Channel channel : articles) {
//	              System.out.println("Id:"+channel.getId()+", Name:"+channel.getChannelName()
//	                      +", UUID: "+channel.getUuid()+", Description:"+channel.getDescription());
//	        }
//	}
//}
//
